#!/usr/bin/env -S awk -f
# Jordan Justen : this file is public domain

BEGIN {
    FS=" +|/";
    t=0;
    p=0;
    f=0;
    ns=0;
    w=0;
}

/Passed:/ { p += $3; t += $4 }

/Failed:/ { f += $3 }

/Not supported:/ { ns += $4 }

/Warnings:/ { w += $3 }

END {
    printf "  Passed:        %d/%d (%.1f%%)\n", p, t, (100.0 * p / t)
    printf "  Failed:        %d/%d (%.1f%%)\n", f, t, (100.0 * f / t)
    printf "  Not supported: %d/%d (%.1f%%)\n", ns, t, (100.0 * ns / t)
    printf "  Warnings:      %d/%d (%.1f%%)\n", w, t, (100.0 * w / t)
}
